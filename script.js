function sumar() {
    var num1 = parseFloat(document.getElementById("num1").value);
    var num2 = parseFloat(document.getElementById("num2").value);
  
    var resultado = num1 + num2;
  
    document.getElementById("resultadoSuma").innerHTML = "El resultado de la suma es: " + resultado;
  }
  
  function calcularArea() {
    var base = parseFloat(document.getElementById("base").value);
    var altura = parseFloat(document.getElementById("altura").value);
  
    var area = (base * altura) / 2;
  
    document.getElementById("resultadoArea").innerHTML = "El área del triángulo es: " + area;
  }
  
  function convertirAFahrenheit() {
    var celsius = parseFloat(document.getElementById("celsius").value);
  
    var fahrenheit = (celsius * 9/5) + 32;
  
    document.getElementById("resultadoFahrenheit").innerHTML = celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.";
  }
  